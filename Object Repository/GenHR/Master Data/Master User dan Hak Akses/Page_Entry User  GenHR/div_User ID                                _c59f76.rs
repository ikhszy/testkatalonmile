<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_User ID                                _c59f76</name>
   <tag></tag>
   <elementGuidId>b4f66ebe-a293-4967-895f-d0bd8f1d7495</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='cpContent_pnl1']/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>row</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    
                        
                            User ID* 
                        
                        
		
			
		
	
                    
                    
                        
                            NIP
                        
                        
		
			
				
			
				
					 -- Bukan Pegawai --00001 - Yulius Rustiandi Oyong00002 - Erwin Purnama00003 - Mohammad Fauzi Ridwan00004 - Yuliana00005 - Asnat Agustine00006 - Yudi Hartadinata00007 - Nico00008 - Anas Abdurrahman00009 - Denny Hermawan00010 - Hari Arryanto01 - Ajay (Udah dibenerin bugsnya)02 - TEST
				
			
		
	
                    
                    
                        
                            Password* 
                        
                        
		
			
		
	
                    
                    
                        
                            Konfirmasi Password* 
                        
                        
		
			
		
	
                    
                    
                        
                            Nama* 
                        
                        
		
			
		
	
                    
                    
                    
                        
                            Peran* 
                        
                        
		
			
				
			
				
					AdministratorFinanceHeadOperator HRDOtorisator HROtorisator HRDPegawai
				
			
		
	
                    
                    
                        
                            Divisi* 
                        
                        
		
			
				
			
				
					PerusahaanDivisiDiv2Dept1
				
			
		
	
                    
                    
                        
                            No Telepon
                        
                        
&lt;!--
(function(){
var a = {'currGroupSeparator':'.','abbrDayNames':['Mgg','Sen','Sel','Rab','Kam','Jum','Sab'],'monthNames':['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember',''],'numGroupSeparator':'.','shortTime':'H:mm','longTime':'H:mm:ss','currDecimalPoint':',','longDate':'dd MMMM yyyy','abbrMonthNames':['Jan','Feb','Mar','Apr','Mei','Jun','Jul','Agu','Sep','Okt','Nov','Des',''],'pm':'','am':'','currPrec':0,'monthDay':'dd MMMM','currency':'Rp','numDecimalPoint':',','percentPattern':1,'dayNames':['Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu'],'genMonthNames':['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember',''],'shortDate':'dd/MM/yyyy'};
for(var b in a) ASPx.CultureInfo[b] = a[b];
})();
ASPx.NumberDecimalSeparator = ',';

//-->

		
			
		
	
                    
                    
                        
                            Email
                        
                        
		
			
		
	
                    
                    
                        
                            Alamat
                        
                        
		
			
		
	
                    
                    Simpan
&lt;!--
ASPx.createControl(dx.BootstrapClientMenu,'cpContent_mnuAction_mnuStraight','',{'uniqueID':'ctl00$cpContent$mnuAction$mnuStraight','serverEvents':['ItemClick'],'renderData':{'':[[0]]}},{'ItemClick':function(s,e) {var selIds = CL_grid1.GetSelectedKeysOnPage();
            var selFirst = true;
            e.idCsv = '';
            for (i = 0; i &lt; selIds.length; i++) {
                if (selFirst) { selFirst = false; } else { e.idCsv = e.idCsv + ','; }
                e.idCsv = e.idCsv + selIds[i].toString();
            } 
GridHandler(s,e); 
 }},null,{'items':[{'name':'Return'}]});
ASPx.createControl(dx.BootstrapClientTextBox,'cpContent_edit_user_id','',{'uniqueID':'ctl00$cpContent$edit_user_id','customValidationEnabled':true,'isValid':true,'errorText':'Invalid value','validationPatterns':[new ASPx.RequiredFieldValidationPattern('Harus Diisi')],'setFocusOnError':true,'isErrorFrameRequired':true,'errorDisplayMode':'i','errorImageIsAssigned':true,'cssClasses':{'nt':'ws_null_text'}});
ASPx.createControl(dx.BootstrapClientListBox,'cpContent_edit_peg_nip_L','',{'uniqueID':'ctl00$cpContent$edit_peg_nip$L','savedSelectedIndex':0,'itemsValue':['','00001','00002','00003','00004','00005','00006','00007','00008','00009','00010','01','02'],'isComboBoxList':true,'cssClasses':{}},{'SelectedIndexChanged':function (s, e) { ASPx.CBLBSelectedIndexChanged('cpContent_edit_peg_nip', e); },'ItemClick':function (s, e) { ASPx.CBLBItemMouseUp('cpContent_edit_peg_nip', e); }});
ASPx.createControl(dx.BootstrapClientComboBox,'cpContent_edit_peg_nip','',{'uniqueID':'ctl00$cpContent$edit_peg_nip','autoCompleteAttribute':{'name':'autocomplete','value':'off'},'lastSuccessValue':null,'islastSuccessValueInit':true,'cssClasses':{}});
ASPx.createControl(dx.BootstrapClientTextBox,'cpContent_edit_user_password','',{'uniqueID':'ctl00$cpContent$edit_user_password','customValidationEnabled':true,'isValid':true,'errorText':'Invalid value','validationPatterns':[new ASPx.RequiredFieldValidationPattern('Harus Diisi')],'setFocusOnError':true,'isErrorFrameRequired':true,'errorDisplayMode':'i','errorImageIsAssigned':true,'cssClasses':{'nt':'ws_null_text'}});
ASPx.createControl(dx.BootstrapClientTextBox,'cpContent_confirm_password','',{'uniqueID':'ctl00$cpContent$confirm_password','cssClasses':{'nt':'ws_null_text'}});
ASPx.createControl(dx.BootstrapClientTextBox,'cpContent_edit_user_nama','',{'uniqueID':'ctl00$cpContent$edit_user_nama','customValidationEnabled':true,'isValid':true,'errorText':'Invalid value','validationPatterns':[new ASPx.RequiredFieldValidationPattern('Harus Diisi')],'setFocusOnError':true,'isErrorFrameRequired':true,'errorDisplayMode':'i','errorImageIsAssigned':true,'cssClasses':{'nt':'ws_null_text'}});
ASPx.createControl(dx.BootstrapClientListBox,'cpContent_edit_user_roles_csv_L','',{'uniqueID':'ctl00$cpContent$edit_user_roles_csv$L','savedSelectedIndex':0,'itemsValue':['Administrator','Finance','Head','Operator HRD','Otorisator HR','Otorisator HRD','Pegawai'],'isComboBoxList':true,'cssClasses':{}},{'SelectedIndexChanged':function (s, e) { ASPx.CBLBSelectedIndexChanged('cpContent_edit_user_roles_csv', e); },'ItemClick':function (s, e) { ASPx.CBLBItemMouseUp('cpContent_edit_user_roles_csv', e); }});
ASPx.createControl(dx.BootstrapClientComboBox,'cpContent_edit_user_roles_csv','',{'uniqueID':'ctl00$cpContent$edit_user_roles_csv','autoCompleteAttribute':{'name':'autocomplete','value':'off'},'lastSuccessValue':'Administrator','islastSuccessValueInit':true,'cssClasses':{}});
ASPx.createControl(dx.BootstrapClientListBox,'cpContent_cboOrg_L','',{'uniqueID':'ctl00$cpContent$cboOrg$L','itemsValue':[1,4,5,6],'isComboBoxList':true,'cssClasses':{}},{'SelectedIndexChanged':function (s, e) { ASPx.CBLBSelectedIndexChanged('cpContent_cboOrg', e); },'ItemClick':function (s, e) { ASPx.CBLBItemMouseUp('cpContent_cboOrg', e); }});
ASPx.createControl(dx.BootstrapClientComboBox,'cpContent_cboOrg','',{'uniqueID':'ctl00$cpContent$cboOrg','autoCompleteAttribute':{'name':'autocomplete','value':'off'},'lastSuccessValue':null,'islastSuccessValueInit':true,'cssClasses':{}});
ASPx.createControl(dx.BootstrapClientSpinEdit,'cpContent_edit_user_telepon','',{'uniqueID':'ctl00$cpContent$edit_user_telepon','heightCorrectionRequired':true,'autoCompleteAttribute':{'name':'autocomplete','value':'off'},'outOfRangeWarningMessages':['The number must be in the range {0}...{1}', 'The number must be greater than or equal to {0}', 'The number must be less than or equal to {0}'],'allowMouseWheel':false,'number':null,'maxLength':20,'cssClasses':{}});
ASPx.createControl(dx.BootstrapClientTextBox,'cpContent_edit_user_email','',{'uniqueID':'ctl00$cpContent$edit_user_email','customValidationEnabled':true,'isValid':true,'errorText':'Invalid value','validationPatterns':[new ASPx.RegularExpressionValidationPattern('Format e-mail salah', '^[_a-z0-9-]+(.[a-z0-9-]+)@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$')],'isErrorFrameRequired':true,'errorDisplayMode':'i','errorImageIsAssigned':true,'cssClasses':{'nt':'ws_null_text'}});
ASPx.createControl(dx.BootstrapClientMemo,'cpContent_edit_user_alamat','',{'uniqueID':'ctl00$cpContent$edit_user_alamat','cssClasses':{}});
ASPx.createControl(dx.BootstrapClientButton,'cpContent_btnSave','',{'autoPostBack':true,'uniqueID':'ctl00$cpContent$btnSave','serverEvents':['Click']});

//-->

                </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;cpContent_pnl1&quot;)/div[@class=&quot;row&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='cpContent_pnl1']/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Return To List'])[1]/following::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Entry User'])[2]/following::div[4]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/div[2]/div/div</value>
   </webElementXpaths>
</WebElementEntity>
