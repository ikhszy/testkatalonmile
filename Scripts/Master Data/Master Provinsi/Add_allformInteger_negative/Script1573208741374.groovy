import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://devpayroll:801/NewPayroll/Public/Login.aspx?ReturnUrl=%2fNewPayroll%2f')

WebUI.click(findTestObject('GenHR/Master Data/Master Provinsi/Page_GenHR - Login/div_Login'))

WebUI.setText(findTestObject('GenHR/Master Data/Master Provinsi/Page_GenHR - Login/input_Login_txtUserId'), 'admin')

WebUI.click(findTestObject('GenHR/Master Data/Master Provinsi/Page_GenHR - Login/div_Password'))

WebUI.setEncryptedText(findTestObject('GenHR/Master Data/Master Provinsi/Page_GenHR - Login/input_Password_txtPassword'), 
    'AHDMALwSiRFPaA+G1mC1uw==')

WebUI.click(findTestObject('GenHR/Master Data/Master Provinsi/Page_GenHR - Login/button_Sign In'))

WebUI.click(findTestObject('GenHR/Master Data/Master Provinsi/Page_Dashboard  GenHR/span_Master Data_arrow'))

WebUI.click(findTestObject('GenHR/Master Data/Master Provinsi/Page_Dashboard  GenHR/span_Master Provinsi'))

WebUI.click(findTestObject('GenHR/Master Data/Master Provinsi/Page_Master Provinsi  GenHR/span_Add'))

WebUI.setText(findTestObject('GenHR/Master Data/Master Provinsi/Page_Entry Provinsi  GenHR/input__ctl00cpContentedit_ref_propinsi_nama'), 
    '123123123123')

WebUI.click(findTestObject('GenHR/Master Data/Master Provinsi/Page_Entry Provinsi  GenHR/button_Simpan'))

WebUI.click(findTestObject('GenHR/Master Data/Master Provinsi/Page_Entry Provinsi  GenHR/button_OK'))

WebUI.closeBrowser()

