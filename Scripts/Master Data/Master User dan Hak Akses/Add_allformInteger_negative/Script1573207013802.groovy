import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://devpayroll:801/NewPayroll/Public/Login.aspx?ReturnUrl=%2fNewPayroll%2f')

WebUI.click(findTestObject('GenHR/Master Data/Master User dan Hak Akses/Page_GenHR - Login/div_Login'))

WebUI.setText(findTestObject('GenHR/Master Data/Master User dan Hak Akses/Page_GenHR - Login/input_Login_txtUserId'), 'admin')

WebUI.click(findTestObject('GenHR/Master Data/Master User dan Hak Akses/Page_GenHR - Login/div_Password'))

WebUI.setEncryptedText(findTestObject('GenHR/Master Data/Master User dan Hak Akses/Page_GenHR - Login/input_Password_txtPassword'), 
    'AHDMALwSiRFPaA+G1mC1uw==')

WebUI.click(findTestObject('GenHR/Master Data/Master User dan Hak Akses/Page_GenHR - Login/button_Sign In'))

WebUI.click(findTestObject('GenHR/Master Data/Master User dan Hak Akses/Page_Dashboard  GenHR/span_Master Data_arrow'))

WebUI.click(findTestObject('GenHR/Master Data/Master User dan Hak Akses/Page_Dashboard  GenHR/span_Master User dan Hak Akses'))

WebUI.click(findTestObject('GenHR/Master Data/Master User dan Hak Akses/Page_Master User dan Hak Akses  GenHR/span_Add'))

WebUI.setText(findTestObject('GenHR/Master Data/Master User dan Hak Akses/Page_Entry User  GenHR/input__ctl00cpContentedit_user_id'), 
    '967975858')

WebUI.click(findTestObject('GenHR/Master Data/Master User dan Hak Akses/Page_Entry User  GenHR/span_NIP_dxbs-edit-btn btn btn-secondary dr_a6eb37'))

WebUI.click(findTestObject('GenHR/Master Data/Master User dan Hak Akses/Page_Entry User  GenHR/span_01 - Ajay (Udah dibenerin bugsnya)'))

WebUI.click(findTestObject('GenHR/Master Data/Master User dan Hak Akses/Page_Entry User  GenHR/div_Password'))

WebUI.setEncryptedText(findTestObject('GenHR/Master Data/Master User dan Hak Akses/Page_Entry User  GenHR/input__ctl00cpContentedit_user_password'), 
    'E8IccTF9o+eWED9TlLGFxA==')

WebUI.click(findTestObject('GenHR/Master Data/Master User dan Hak Akses/Page_Entry User  GenHR/div_Konfirmasi Password'))

WebUI.setEncryptedText(findTestObject('GenHR/Master Data/Master User dan Hak Akses/Page_Entry User  GenHR/input__ctl00cpContentconfirm_password'), 
    'E8IccTF9o+eWED9TlLGFxA==')

WebUI.setText(findTestObject('GenHR/Master Data/Master User dan Hak Akses/Page_Entry User  GenHR/input__ctl00cpContentedit_user_nama'), 
    '967975858')

WebUI.click(findTestObject('GenHR/Master Data/Master User dan Hak Akses/Page_Entry User  GenHR/span__dxbs-edit-btn btn btn-secondary dropd_55a405'))

WebUI.click(findTestObject('GenHR/Master Data/Master User dan Hak Akses/Page_Entry User  GenHR/span_Administrator'))

WebUI.click(findTestObject('GenHR/Master Data/Master User dan Hak Akses/Page_Entry User  GenHR/span__dxbs-edit-btn btn btn-secondary dropd_55a405_1'))

WebUI.click(findTestObject('GenHR/Master Data/Master User dan Hak Akses/Page_Entry User  GenHR/span_Perusahaan'))

WebUI.setText(findTestObject('GenHR/Master Data/Master User dan Hak Akses/Page_Entry User  GenHR/input_No Telepon_ctl00cpContentedit_user_telepon'), 
    '967975858')

WebUI.setText(findTestObject('GenHR/Master Data/Master User dan Hak Akses/Page_Entry User  GenHR/input_Email_ctl00cpContentedit_user_email'), 
    '967975858')

WebUI.setText(findTestObject('GenHR/Master Data/Master User dan Hak Akses/Page_Entry User  GenHR/textarea_Alamat_ctl00cpContentedit_user_alamat'), 
    '967975858')

WebUI.click(findTestObject('GenHR/Master Data/Master User dan Hak Akses/Page_Entry User  GenHR/button_Simpan'))

WebUI.click(findTestObject('GenHR/Master Data/Master User dan Hak Akses/Page_Entry User  GenHR/button_OK'))

WebUI.closeBrowser()

