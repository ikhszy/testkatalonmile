import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('http://devpayroll:801/NewPayroll/Public/Login.aspx?ReturnUrl=%2fNewPayroll%2f')

WebUI.click(findTestObject('GenHR/Master Data/Tarif Pajak/Tarif PTKP/Page_GenHR - Login/div_Login'))

WebUI.setText(findTestObject('GenHR/Master Data/Tarif Pajak/Tarif PTKP/Page_GenHR - Login/input_Login_txtUserId'), 'admin')

WebUI.click(findTestObject('GenHR/Master Data/Tarif Pajak/Tarif PTKP/Page_GenHR - Login/div_Password'))

WebUI.setEncryptedText(findTestObject('GenHR/Master Data/Tarif Pajak/Tarif PTKP/Page_GenHR - Login/input_Password_txtPassword'), 'AHDMALwSiRFPaA+G1mC1uw==')

WebUI.click(findTestObject('GenHR/Master Data/Tarif Pajak/Tarif PTKP/Page_GenHR - Login/button_Sign In'))

WebUI.click(findTestObject('GenHR/Master Data/Tarif Pajak/Tarif PTKP/Page_Dashboard  GenHR/span_Master Data'))

WebUI.click(findTestObject('GenHR/Master Data/Tarif Pajak/Tarif PTKP/Page_Dashboard  GenHR/span_Tarif Pajak'))

WebUI.click(findTestObject('GenHR/Master Data/Tarif Pajak/Tarif PTKP/Page_Dashboard  GenHR/span_Tarif PTKP'))

WebUI.click(findTestObject('GenHR/Master Data/Tarif Pajak/Tarif PTKP/Page_Tarif PTKP  GenHR/span_Add'))

WebUI.click(findTestObject('GenHR/Master Data/Tarif Pajak/Tarif PTKP/Page_Entry Tarif PTKP  GenHR/span_Tahun_dxbs-edit-btn btn btn-secondary _a0248d'))

WebUI.click(findTestObject('GenHR/Master Data/Tarif Pajak/Tarif PTKP/Page_Entry Tarif PTKP  GenHR/span_2015'))

WebUI.setText(findTestObject('GenHR/Master Data/Tarif Pajak/Tarif PTKP/Page_Entry Tarif PTKP  GenHR/input__ctl00cpContentedit_tPTKP_WP'), 
    '10000')

WebUI.setText(findTestObject('GenHR/Master Data/Tarif Pajak/Tarif PTKP/Page_Entry Tarif PTKP  GenHR/input__ctl00cpContentedit_tPTKP_Kawin'), 
    '100000')

WebUI.setText(findTestObject('GenHR/Master Data/Tarif Pajak/Tarif PTKP/Page_Entry Tarif PTKP  GenHR/input__ctl00cpContentedit_tPTKP_Tanggungan'), 
    '100000')

WebUI.setText(findTestObject('GenHR/Master Data/Tarif Pajak/Tarif PTKP/Page_Entry Tarif PTKP  GenHR/input__ctl00cpContentedit_tPTKP_Tanggungan_max'), 
    '2')

WebUI.click(findTestObject('GenHR/Master Data/Tarif Pajak/Tarif PTKP/Page_Entry Tarif PTKP  GenHR/button_Simpan'))

WebUI.click(findTestObject('GenHR/Master Data/Tarif Pajak/Tarif PTKP/Page_Entry Tarif PTKP  GenHR/button_OK'))

